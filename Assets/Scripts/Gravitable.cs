﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravitable : MonoBehaviour
{
    /*
     * Correspondence between the indexes in the forces array and the assigned force
     * 0:   gravity
     * 1:   horizontal impulse
     * 2:   jump impulse
     * 3:   air friction
     * 4:   floor friction (TODO)
     */

    //Character properties
    public float characterMass = 3;
    public float minimalSpeedValue = 0.1f;
    public float horizontalMoveForce = 50;
    public Vector2 initialJumpForce = new Vector2(0, 500); //This is a vector because we want to be able to invert the jump direction according to the gravity
    //Environement properties
    public Vector2 initialGravity = new Vector2(0, -80); //This is a vector because we want to be able to invert the gravity
    public float airDragValue = 3;
    public float floorFrictionValue = 20;

    private Vector2[] forces = new Vector2[10];
    private Vector2 speed = new Vector2(0, 0);

    public bool isGrounded = false;
    private bool isJumping = false;

    public float jumpPulseDuration = 0.1f; //jump pulse duration in seconds (without this, the character would jump higher when the frames are longer) 
    float jumpPulseTimer = 0f;

    RaycastHit2D hit;
    bool freeze = false;

    //Update Speed: This function will update the current speed according to the forces being applied on the character
    Vector2 UpdateSpeed(Vector2 currentSpeed)
    {
        Vector2 retSpeed = new Vector2(0, 0);
        Vector2 sumForces = new Vector2(0, 0);

        for(int i=0; i<forces.Length; i++)
        {
            sumForces += forces[i];
        }
        // Fundamental principle of Dynamic: sumForces = characterMass * Acceleration
        retSpeed = currentSpeed + (sumForces/ characterMass) * Time.deltaTime;

        //If there are no forces and the speed is small enough we just cancel it (to avoid slow perpetual motion caused by integration errors)
        if (retSpeed.magnitude < minimalSpeedValue && sumForces == Vector2.zero)
        {
            retSpeed = Vector2.zero;
        }
        return retSpeed;
    }

    void Update()
    {
            //limit the speed with air friction
            //The air friction is proportionnal to the speed by the airDragValue coefficient
            if (speed.magnitude > minimalSpeedValue)
            {
                forces[3] = -speed * airDragValue;
            }
            else
            {
                forces[3] = Vector2.zero;
            }

            //floor friction
            //only active when the player don't give direction input or when he changes direction(speed opposed to direction input) (because the friction of the floor when whe are walking does not slow us down)
            if (isGrounded && (forces[1] == Vector2.zero || Vector2.Dot(speed, new Vector2(Input.GetAxisRaw("Horizontal"), 0)) < 0))
            {
                forces[4] = -speed * floorFrictionValue;
            }
            else
            {
                forces[4] = Vector2.zero;
            }
            // gravity
            if (!isGrounded)
            {
                // make the character fall downward
                forces[0] = initialGravity;
            }
            else
            {
                //Stop making the character fall because it's now on the ground
                forces[0] = Vector2.zero;
            }

            //move the character horizontaly
            forces[1] = new Vector2(horizontalMoveForce * Input.GetAxisRaw("Horizontal"), 0);

            //jump
            if (!isJumping && Input.GetKeyDown(KeyCode.Space))
            {
                isJumping = true;
                jumpPulseTimer = Time.time;
                forces[2] = initialJumpForce;
                speed = new Vector2(speed.x, 0); // we reset vertical speed at the beginnning of the jump for a more "juicy" behaviour in case of double jump
            }
            else
            {
                if (Time.time - jumpPulseTimer >= jumpPulseDuration)
                {
                    forces[2] = Vector2.zero;
                }
            }

            //update the speed
            speed = UpdateSpeed(speed);
        /*
            hit = Physics2D.BoxCast(gameObject.transform.position, new Vector2(0.32f, 0.32f), 0, speed);
            if (Vector2.Distance(gameObject.transform.position, hit.transform.position) < Vector2.SqrMagnitude(speed) + GetComponent<Renderer>().bounds.size.y / 2)
            {
                /*Debug.Log(Vector2.Distance(gameObject.transform.position, hit.transform.position));
                Debug.Log(Vector2.SqrMagnitude(speed) + GetComponent<Renderer>().bounds.size.y / 2);*/
                /*
                Debug.Log(hit.transform.position);
                Debug.Log(hit.point);
                if (hit.transform.position.y < gameObject.transform.position.y)
                {
                    transform.position = hit.point + new Vector2(0,  0.52f);
                    Debug.Log(hit.point + new Vector2(0, 0.52f));
                }
                else
                {
                    transform.position = hit.point - new Vector2(0, 0.52f);
                    Debug.Log(hit.point - new Vector2(0, 0.52f));
                }

            }
        */

            transform.position += new Vector3(speed.x, speed.y, 0) * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        // TODO: We need to test if we effectivelly hit a platform object (use of tags)
        float characterHeight = GetComponent<Renderer>().bounds.size.y;
        float platformHeight = other.GetComponent<Renderer>().bounds.size.y;

        //We hit the ground (need to be tested with tags) from above
        if (transform.position.y > other.transform.position.y)
        {
            isGrounded = true;
            isJumping = false;
            transform.position = new Vector3(transform.position.x, other.transform.position.y + (characterHeight + platformHeight) / 2, 0);
        }
        //We hit the ground (need to be tested with tags) from below
        else
        {
            transform.position = new Vector3(transform.position.x, other.transform.position.y - (characterHeight + platformHeight) / 2, 0);
            
        }
        //In both cases we need to set the vertical speed to zero
        speed = new Vector2(speed.x, 0);

    }

    void OnTriggerExit2D(Collider2D other)
    {
        // This test prevent the character to jump if he fell (without jumping) from a platform (isJumping = true au lieu de = false)
        /*note: j'aime bien le fait de pouvoir sauter après être tombé dans le vide dans une plateforme
        A vrai dire je préfère ce comportement qu'un double saut car je le trouve plus intéressant:
        On pourrait imaginer une paire de plateforme A et B où pour pouvoir atteindre B le joueur doit obligatoirement se laisser tomber de A puis sauter
        Cela donne selon moi des mécaniques de gameplay bien plus intéressantes que le double saut*/
        
        if (!isJumping)
        {
            isJumping = true;
        }

        // TODO: We need to test if we effectivelly quit a platform object maybe (use of tags) or we will use the collision matrix.
        isGrounded = false;
    }
}
