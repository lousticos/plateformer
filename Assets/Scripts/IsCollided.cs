﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsCollided : MonoBehaviour
{
    public bool isCollided = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isCollided = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isCollided = false;
    }
}
