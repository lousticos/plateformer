﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollider : MonoBehaviour
{
    public float movementForce = 0f;
    public bool grounded = false;
    public Renderer rend;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!grounded)
        {
            gameObject.transform.position -= new Vector3(0, 0.05f);
        }
        float hor = Input.GetAxisRaw("Horizontal");
        gameObject.transform.position += new Vector3(hor / 10, 0);
        if (Input.GetKey(KeyCode.Space))
        {
            gameObject.transform.position += new Vector3(0, 0.1f);
        }
    }

    public void Move(float nspeed)
    {
        movementForce = nspeed;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.transform.position.x - collision.gameObject.GetComponent<Renderer>().bounds.size.x / 2);
        Debug.Log(transform.position);
        if(transform.position.x < collision.transform.position.x - collision.gameObject.GetComponent<Renderer>().bounds.size.x / 2)
        {
            Debug.Log("side !");
        }
        else
        {
            
            if(transform.position.y < collision.transform.position.y)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, collision.gameObject.transform.position.y - (collision.gameObject.GetComponent<Renderer>().bounds.size.y / 2 + rend.bounds.size.y / 2));

            }
            else
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, collision.gameObject.transform.position.y + collision.gameObject.GetComponent<Renderer>().bounds.size.y / 2 + rend.bounds.size.y / 2);
                grounded = true;
            }
        }



    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        grounded = false;
    }
}


